var fs = require("fs");
var jsonFile = "tasks.json";

function getTasks(callback) {
    fs.readFile(jsonFile, { encoding: "utf8" }, function(err, data) {
        if (err) {
            return callback(err);
        } else {
            return callback(null, JSON.parse(data));
        }
    });
}

function saveTasks(tasks, callback) {
    fs.writeFile(jsonFile, JSON.stringify(tasks), { enconding: "utf8" }, callback);
}

module.exports = {
    getTasks: getTasks,
    saveTasks: saveTasks
}