var express = require("express");
var bodyParser = require("body-parser");
var db = require("./db")
var app = express();

app.use(express.static("client"));
app.use(bodyParser.urlencoded({ extended: false }));

// Middleware
function loadTasks(req, res, next) {
    db.getTasks(function(err, data) {
        if (err) {
            next(err);
        } else {
            req.tasks = data;
            next();
        }
    });
}

function addTask(req, res, next) {
     var task = {
        id: new Date().getTime(),
        title: req.body.title,
        completed: false
    };
    
    req.tasks.push(task);
    req.task = task;
    next();
}

function updateTask(req, res, next) {
    var taskId = parseInt(req.body.id);
    
    req.tasks.some(function(task, index) {
        if (task.id === taskId) {
            task.title = req.body.title;
            task.completed = req.body.completed == "true";
            req.task = task;
            return true;
        }
    });
    
    next();
}

function deleteTask(req, res, next) {
    var taskId = parseInt(req.params.id);
    
    req.tasks.some(function(task, index) {
        if (task.id === taskId) {
            req.tasks.splice(index, 1);
            req.task = task;
            return true;
        }
    });
    
    next();
}

function saveTasks(req, res, next) {
    if (!req.task || !req.tasks) {
        res.sendStatus(404);
    } else {
        db.saveTasks(req.tasks, function(err) {
            if (err) {
                next(err);
            } else {
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(req.task));
            }
        });
    }
}

// Routes
app.get("/tasks", loadTasks, function(req, res) {
    res.set('Content-Type', 'application/json');
    res.send(JSON.stringify(req.tasks));
});
app.post("/tasks", loadTasks, addTask, saveTasks);
app.put("/tasks", loadTasks, updateTask, saveTasks);
app.delete("/tasks/:id", loadTasks, deleteTask, saveTasks);

var server = app.listen(3000, function() {
    console.log("Test server listening at http://localhost:%s", server.address().port);
});